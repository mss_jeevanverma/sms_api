<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use ReflectionClass;
use ReflectionMethod;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component\FlashComponent;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class IndexController extends AppController
{
    public function initialize()
    {
        $this->viewBuilder()->layout('index');
    }
    
    public $components = array('Auth');
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //$this->Auth->allow('add');
         // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow();
    }

 public function add()
    {
    
      $this->loadModel('Users');
        $session = $this->request->session();
        $msg ='';
        $this->loadModel('Role');
        $role=$this->Role->find('all')->toArray();
        if($this->request->is('post')){
            $post_data = $this->request->data;
            $email = $this->Users->find('all')->where(['email =' => $post_data['email']])->toArray();
            if(!count($email)){
                $post_data['password'] = md5($post_data['password']);
                $post_data['status'] = 1;
                $connection = ConnectionManager::get('default');
                $connection->insert('users',$post_data);
                $this->redirect(['controller'=>'index','action'=>'index']);
               }
                else{
                $msg="Email already Exists";
                die($msg);
            }
        }
        $this->set('role', $role);
    }

   

    public function index()
         {
    $this->loadModel('Role');
       $this->loadModel('Users');
        $session = $this->request->session();
        if ($this->request->is('post')) {
            $post_data=$this->request->data;
            $email=$this->Users->find('all')->where(['email'=>$post_data['email']])->toArray();
                                 $email_len=$email[0]->email;

                    if(strlen($email_len)>3)
                    {                    
                         if(md5($post_data['password'])==$email[0]->password)
                         {
                        $UserInfo = array(
                        "id"=>$email[0]->id,
                        "role"=>$email[0]->role,
                        "email"=>$email[0]->email,
                        "first_name"=>$email[0]->first_name,
                        "last_name"=>$email[0]->last_name,
                        "status"=>$email[0]->status,
                        "user_name"=>$email[0]->user_name
                       

                        );
                      if(!$UserInfo['status'])
                        {
                            $msg = "Your Account is Not Activated.";   
                            die($msg);    
                        }
                        else{
                                unset($UserInfo["password"]);
                                if($UserInfo['role']=='admin')
                                {            
                                  $session->write('UserInfo', $UserInfo);                                                                                    
                                   $this->redirect(array("controller" => "admin", "action" => "index")); 
                                }
                                else if($UserInfo['role']=='guest')
                                {
                                    $session->write('UserInfo', $UserInfo);
                                    $this->redirect(array("controller" => "admin", "action" => "index")); 
                                }
                            }


                             }   
                    }
                    else
                    {
                        $msg = "Eather Email or Password Does Not Match.";
                    }



        }
    }

   public function logout()
    {
         $session = $this->request->session();
        $session->delete('UserInfo');
        $this->redirect(array("controller" => "index", "action" => "index"));
    }



  

}
