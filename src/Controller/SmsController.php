<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

require('Services/Twilio.php');

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SmsController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function index()
    {
        if($this->request->is('post')){    

            if(isset($_POST['phone_no'])){

                $random_code = rand(100,999).rand(10,99).rand(100,999);
                $applicants = TableRegistry::get('SmsCode');
                $query = $applicants->query();
                $query_status = $query->insert(['phone_no','sms_code','status'])
                  ->values(['phone_no'=>$_POST['phone_no'],'sms_code'=>$random_code,'status'=>$random_code])
                  ->execute();

                if($query_status){
                    die(json_encode(array("status"=>"success","msg"=>"Code Genrated Successfully")));
                }else{
                    die(json_encode(array("status"=>"error","msg"=>"Something went wrong try again later")));
                }

            }else{

                die(json_encode(array("status"=>"error","msg"=>"Phone No Empty")));
            }
        }
        exit;
    }

    public function genratecode(){

        if($this->request->is('post')){    

            if(isset($_POST['phone_no'])){

                $random_code = rand(100,999).rand(10,99).rand(100,999);
                $applicants = TableRegistry::get('SmsCode');
                $query = $applicants->query();
                date_default_timezone_set('Asia/Calcutta');
                $query_status = $query->insert(['phone_no','sms_code','status','last_updated'])
                  ->values(['phone_no'=>$_POST['phone_no'],'sms_code'=>$random_code,'status'=>0,'last_updated'=>date("Y-m-d H:i:s")])
                  ->execute();

                if($query_status){
                    $account_sid = 'ACb314428e94fb60a511e05ae4e99f2a69'; 
                    $auth_token = '9ed2d9267b3894230cb4ca8fbf627d00'; 
                    $client = new \Services_Twilio($account_sid, $auth_token); 

                    $client->account->messages->create(array( 
                        'To' => $_POST['phone_no'], 
                        'From' => "+15635185238", 
                        'Body' => "Your Varification Code is ".$random_code.". It will expire within 15 minutes.",   
                    ));
        
                    die(json_encode(array("status"=>"success","msg"=>"Code Genrated Successfully")));
                }else{
                    die(json_encode(array("status"=>"error","msg"=>"Something went wrong try again later")));
                }

            }else{

                die(json_encode(array("status"=>"error","msg"=>"Wrong Parameters")));
            }
        }
        exit;
    }

    public function verifycode(){
        $this->loadModel('SmsCode');

        if($this->request->is('post')){    

            if(isset($_POST['phone_no']) && isset($_POST['sms_code'])){

                $phone_data = $this->SmsCode->find('all')->where(['phone_no' => $_POST['phone_no']])->toArray();    

                if(count($phone_data)){

                    if($phone_data[0]['sms_code']==$_POST['sms_code']){

                        date_default_timezone_set('Asia/Calcutta');
                        $dateTimeNow = strtotime(date("Y-m-d H:i:s"));
                        $lastUpdated = strtotime($phone_data[0]['last_updated']);
                        $diffInMins = round(abs( $dateTimeNow -$lastUpdated ) / 60,2);

                        if($diffInMins<15){

                            $this->SmsCode->deleteAll(array("SmsCode.id" => $phone_data[0]['id'] ));
                            die(json_encode(array("status"=>"success","msg"=>"Code Used")));    

                        }else{
                            die(json_encode(array("status"=>"error","msg"=>"Code has expired please genrate new code")));    
                        }

                    }else{
                        die(json_encode(array("status"=>"error","msg"=>"Code Does not match")));    
                    }

                }else{
                    die(json_encode(array("status"=>"error","msg"=>"Invalid Phone Number")));                    
                }


            }else{

                die(json_encode(array("status"=>"error","msg"=>"Wrong Parameters")));
            }

        }

        exit;
    }
}
