<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use ReflectionClass;
use ReflectionMethod;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Controller\Component\FlashComponent;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AdminController extends AppController
{
    public static $all_controllers=array();
    public function initialize()
    {
        $this->viewBuilder()->layout('admin');
        $session=$this->request->session();
        if($session->read('UserInfo')){
            $user_info=$session->read('UserInfo');
            if($user_info['role']!='admin'){
                 $this->redirect(array("controller" => "admin", "action" => "index"));     
            }
          }
            else{
                 $this->redirect(array("controller" => "admin", "action" => "index")); 
            }

      }
    
    public function index()
    {
 
    } 
    public function addTask()
    {
        $this->loadModel('Tasks');
        $session = $this->request->session();
        $msg ='';
       
        if($this->request->is('post')){
            $post_data = $this->request->data;
           $task_name=$this->Tasks->find('all')->where(array('task_name =' => $post_data["task_name"]))->toArray();
         if(count($task_name)){
         $msg = "Taskname name already exist";
         die($msg);
             }else {
                $connection = ConnectionManager::get('default');
                $connection->insert('tasks',$post_data);
                $this->redirect(['controller'=>'index','action'=>'index']);
                $msg="Task create successfully";
                die($msg);
               }
           }
              }
    public function allTask()
    {
       $this->loadModel('Tasks');
       $this->paginate = array('limit' => 10,'order' => array('id' => 'desc'));

            $all_user = $this->paginate('Tasks')->toArray();
            $data = array ("all_user"=>$all_user);
           // $this->set('all_user',$all_user);
            if($this->request->is('get'))
            {
              @$id=$this->request->query['id'];
                @$status=$this->request->query['status'];
               
                if($status) {
                    $status_arr = array("status"=>0);
                    } 
                else {
                      $status_arr = array("status"=>1);
                     }
           $this->Tasks->updateAll($status_arr,  array('id' => $id ));
                     
         }
            $this->set('data', $data);
    }

    public function editTask()
    {
            $this->loadModel('Tasks');
            @$id=$this->request->query['id'];

            if($this->request->is('post'))
            {
            $post_data=$this->request->data; 
            $task_name=$this->Tasks->find('all')->where(array('task_name =' => $post_data["task_name"],'id !='=>$id))->toArray();
         if(count($task_name)){
         $msg = "Taskname name already exist";
         die($msg);
             }
        else
           {
         $this->Tasks->updateAll($post_data,array('id' => $id));
         $msg = "Task successfully updated";
         die($msg);
         }

            }
            
            $data=$this->Tasks->find('all')->where(['id'=>$id])->toArray();
            $this->set('data', $data);  
   }

   


   

}
