<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

?>
<div class="row">
     <div class="col-lg-5 col-lg-offset-3">
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create('User', array('id'=>'sign_in', 'data-toggle'=>'validator')) ?>
    <fieldset>
        <legend><?= __('Please Enter your Email and Password') ?></legend>
        <br />
        <div class="form-group">
        <?= $this->Form->input('email',array(
    'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'data-error="Email address is invalid"','maxlength'=>'100','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'Enter email')) ?>
    <div class="help-block with-errors"></div>
</div>
 <div class="form-group">
        <?= $this->Form->input('password',array(
    'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','placeholder'=>'Password')) ?>
     <div class="form-group">
    </fieldset>
<?= $this->Form->button(__('Login')); ?>
<?= $this->Form->end() ?>
</div>
</div>
