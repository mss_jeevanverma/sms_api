<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

foreach($role as $role)
    {                                                       
        $data[$role->name]=$role->name;              
    }
?>
<div class="row">
     <div class="col-lg-6 col-lg-offset-6">
<?= $this->Form->create('register',array('class'=>'account_fm_rt account_fm fv-form fv-form-bootstrap')) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <br />
	<?= $this->Form->input('user_name',array(
    'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','pattern'=>'[a-zA-Z\s]+','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'User Name'))  ?>      
        <?=$this->Form->input('first_name',array(
    'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','pattern'=>'[a-zA-Z\s]+','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'First Name')) ?>
        <?= $this->Form->input('last_name',array(
    'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','pattern'=>'[a-zA-Z\s]+','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'Last Name')) ?>
        <?= $this->Form->input('email' ,array(
    'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'Email')) ?>
        <?= $this->Form->input('password' ,array(
    'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'Password')) ?>
        <?= $this->Form->input('role',array(
            'options' =>$data,
            'label' => false,
			'type'    => 'select',
       		'empty'   => false,
       		

            )
        ) ?>
          
   </fieldset>
   <br/>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end() ?>
</div>
</div>