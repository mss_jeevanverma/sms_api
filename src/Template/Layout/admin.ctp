<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->script('jquery-1.9.1.min.js') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
     <?= $this->Html->script('formValidation.min.js') ?>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
 <?php  $session=$this->request->session();
$user_info=$session->read('UserInfo');
// echo '<pre>';
// print_r($user_info['permissions']);

  ?>
    <section class="appointment">
     <div class="container">
        <div class="row">
                    <div class="col-sm-12 heding_tab"><h2 class=""></h2></div>
                                           <div class="btn-group" role="group" aria-label="...">
                  <button type="button" class="btn btn-default">Dashboard</button>
                  

                  <div class="btn-group" role="group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Tasks 
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a href="<?php echo $base_url_temp; ?>admin/add_task">Add Tasks</a></li>
                      <li><a href="<?php echo $base_url_temp; ?>admin/all_task">All Tasks</a></li>
                    </ul>
                  </div>
                   <div class="btn-group btn btn-default">
                    
                      <a href="<?php echo $base_url_temp; ?>index/logout">Logout</a>
                                    
                    </div>
                  </div>
                </div>
        </div>
    <?= $this->fetch('content') ?>
    </div>
    </section>
    <footer>
    </footer>
</body>
</html>
