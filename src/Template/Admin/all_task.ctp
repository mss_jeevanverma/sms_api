<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


?>

<?php
    $user_info = $this->request->session()->read('UserInfo');
    $all_user = @$data["all_user"];
    $paginator = $this->Paginator;
?>

         
                               

<div class="row">
                   <div class="row">
                            <div  class="col-lg-8 col-md-offset-2">
                                <div class="manage-heros-table">
                                    <div class="panel panel-default">
                                      <!-- Default panel contents -->
                                   
                                        <table  class="table">
                                        <thead>
                                            <tr>
                                                <th style="width:80px;">S No.</th>
                                                <th  style="width:150px;">Task Name</th>
                                                <th  style="width:150px;">Description</th>
                                               
                                                <th  style="width:100px;">Status</th>
                                               
                                                <th  style="width:150px;">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $sr_no=1; 
                                        foreach($all_user as  $all_user) { ?>
                                            <tr>
                                                <td><?php echo $sr_no; ?></td>
                                                <td><?php echo $all_user->task_name; ?></td>
                                                <td><?php echo $all_user->task_description; ?></td>
                                                <td><?php if($all_user->status) {echo "Active"; }else{echo "Not Active";} ?></td>
                                                <!--td><?php echo $hero->phone_no; ?></td-->
                                                 <td>
                                                    <a href="<?php echo $base_url_temp;?>admin/edit_task?id=<?php echo $all_user->id; ?>">
                                                         Edit
                                                    </a> / 
                                                    <!--<a class="a_delete" href="<?php echo $base_url_temp; ?>admin_dashboard/edithero/" data="id=<?php echo $hero->id; ?>&del=yes" >
                                                        Delete
                                                    </a> / -->
                                                    <a class="status_active" href="<?php echo $base_url_temp;?>admin/all_task/" data="id=<?php echo $all_user->id ?>&status=<?php echo $all_user->status; ?>" >
                                                        <?php if ($all_user->status) { ?> 
                                                           Deactivate
                                                        <?php }else{ ?>
                                                             Activate
                                                        <?php } ?>
                                                    </a>

                                                </td>
                                            </tr>
                                        <?php $sr_no++; } ?>    
                                        </tbody>
                                        </table>
                                     </div>
                                 </div>
                            </div>
                 </div>
    <center>
        <div class='pagination'>
        <?php 
            echo $paginator->first("First");
             
            if($paginator->hasPrev()){
                echo $paginator->prev("Prev");
            }
            echo $paginator->numbers(array('modulus' => 2));
            if($paginator->hasNext()){
                echo $paginator->next("Next");
            }
            echo $paginator->last("Last");
        ?>
        </div>




    </center>
</div>


  <script type="text/javascript">
  $(document).ready(function() {
   $(".status_active").click(function(event){
        event.preventDefault();
        var data=$(this).attr('data');
        var url=$(this).attr('href');
        $.ajax({
            type:"GET",
            url:url,
            data:data,
            success: function(result){
                window.location.href = window.location.href;
            },
            error: function(e){}
        });

        });
        });


  </script>          


