<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


    
?>
<div class="row">
     <div class="col-lg-5 col-lg-offset-3">
<?= $this->Form->create('register',array('id'=>'add_user','class'=>'account_fm_rt account_fm fv-form fv-form-bootstrap')) ?>
    <fieldset>
        <div class="for_succ"></div>
        <legend><?= __('Add Task') ?></legend>
        <br />
                    <div class="form-group">
            	<?= $this->Form->input('task_name',array(
                'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','pattern'=>'[a-zA-Z\s]+','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'Task Name'))  ?>
                <div class="help-block with-errors"></div>
            </div>      
                   <div class="form-group"> 
                    <?=$this->Form->input('task_description',array(
                'div'=>false,'class' => 'form-control input-lg','label' => false,'required'=>'true','data-error'=>'Only Character and Minimum Length 1','maxlength'=>'100','pattern'=>'[a-zA-Z\s]+','data-minlength'=>'1','maxlength'=>'50','placeholder'=>'Task Description')) ?>
                <div class="help-block with-errors"></div>
               </div>
                   
                          
          
   </fieldset>
   <br/>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end() ?>
</div>
</div>


<script type="text/javascript">

$(document).ready(function(){
  $('#add_user').on("submit",function(event){
     event.preventDefault();
     var data=$('#add_user').serialize();
           $.ajax({  
            data:data,
            type:'POST',
            url:'<?php echo $base_url_temp; ?>'+'admin/add_task',
             success:function(result){
                console.log(result);
                if(result=='Task create successfully'){
                    $(".for_succ").html('<div role="alert"  class="alert alert-success" >Task create successfully</div>');
                    setTimeout(function(){
                      $('.for_succ').html('');
                    }, 2000);

                }
               
                else if(result=='Taskname name already exist'){
                    $(".for_succ").html('<div role="alert"  class="alert alert-danger" >Taskname name already exist</div>');
                    setTimeout(function(){
                      $('.for_succ').html('');
                    }, 2000);
                }
                },
                error:function(error){
                    console.log(error);
                }

                });



          });
        });

</script>
