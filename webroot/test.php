<?php
    abstract class Lister
    {
        abstract function listing();

        abstract function logAccess( $message );

    } /*** end of class ***/


    abstract class baseLister extends Lister
    {
        /*** a few variables ***/
        public $path, $message;

        /*** define default behaviors ***/
        public function listing()
        {
            echo 'list all directories';
        }

        /*** define default log message ***/
        public function logAccess( $message )
        {
           // echo 'default log message';
        }
    } /*** end of class ***/


    class fileLister extends baseLister
    {
        /*** a few variables ***/
        public $path, $message;

        public function __construct( $path )
        {
            /*** set the directory name ***/
            $this->path = $path;
        }

        /*** override  the parent listing method with our own ***/
        public function listing()
        {
            $it = new directoryIterator( $this->path );
            while( $it->valid() )
            {
                if( $it->isFile() )
                {
                    echo $it->current().'<br />';
                }
                $it->next();
            }
        }

        // /*** override  the parent logAccess Message with our own ***/
        // public function logAccess( $message )
        // {
        //     echo 'Logging File Listing<br />';
        // }
    }

    try
    {
        $obj = new fileLister('./');
        $obj->listing();
        $obj->logAccess('File Listing');

    }
        catch(Exception $e)
    {
echo $e->getMessage();
}


